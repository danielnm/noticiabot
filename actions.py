###############################################################
# Programa: busca de fontes RSS para bot de notícias no Rasa
# Autor: Daniel Nehme Muller
# Criado em: agosto/2020
# Licença: mesma do Rasa
# Arquivo: actions.py - forms e actions do bot
###############################################################

from typing import Any, Text, Dict, List, Union

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction
from rasa_sdk.events import SlotSet

# classes para pegar RSS
import feedparser
from markdownify import markdownify

##################### Define fontes de notícias
class Fontes():
    def __init__(self):
        Fontes.geral = 'http://rss.uol.com.br/feed/noticias.xml'
        Fontes.economia = 'http://rss.uol.com.br/feed/economia.xml'
        Fontes.tecnologia = 'http://rss.uol.com.br/feed/tecnologia.xml'
        # Este recebe a fonte atual:
        Fontes.fonte = None

##################### Formulário para escolha do tipo de notícias
class TipoNoticiaForm(Fontes,FormAction):
    """Pega o tipo de notícia"""

    ##### Inicializa classe e herda parâmetros de Fontes
    def __init__(self):
        Fontes.__init__(self)

    ##### Nome do form para o Rasa
    def name(self) -> Text:
        """Form de tipo de notícia."""
        return "tipo_noticia_form"

    ##### Diz que slot é de preenchimento obrigatório
    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A opção a preencher: tipo de notícia."""
        return ["escolha_tipo_noticia"]

    # Mapeia intenção para nome do tipo
    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """Mapeamento de uma intenção de notícia para um determinado tipo."""
        return {
            "escolha_tipo_noticia": [
                self.from_intent(intent="noticia_geral", value="geral"),
                self.from_intent(intent="noticia_economia", value="economia"),
                self.from_intent(intent="noticia_tecnologia", value="tecnologia"),
                self.from_text(intent=None),
            ],
        }

    ##### Validação dos tipos de notícia
    def validate_escolha_tipo_noticia(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Valida opção de tipo de notícia e atribui a fonte."""
        if value == "geral":
            Fontes.fonte = feedparser.parse(Fontes.geral)
        elif value == "economia":
            Fontes.fonte = feedparser.parse(Fontes.economia)
        elif value == "tecnologia":
            Fontes.fonte = feedparser.parse(Fontes.tecnologia)
        else:
            dispatcher.utter_message(template="utter_opcao_estranha_tipo")
            return {"escolha_tipo_noticia": None}

        return {"escolha_tipo_noticia": value}

    ##### Saída do form
    def submit(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict]:
        """Saída do form."""
        return []


##################### Formulário para mostrar notícias
class MancheteForm(Fontes,FormAction):
    """Pega o número da manchete"""

    ##### Inicializa classe e herda parâmetros de Fontes
    def __init__(self):
        Fontes.__init__(self)

    ##### Nome do form para o Rasa
    def name(self) -> Text:
        """Identifica o form na estória do Rasa."""

        return "manchete_form"

    ##### Diz que slot é de preenchimento obrigatório
    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A opção a preencher."""

        return ["mostra_manchetes"]

    ##### Mapeia para intenção de números
    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """Mapeamento da intenção de números."""
        return {
            "mostra_manchetes": [
                self.from_text(intent="numeros"),
                self.from_text(intent=None),
            ],
        }

    ##### Só para ter certeza que o texto representa um número
    @staticmethod
    def is_int(string: Text) -> bool:
        """Confere se é um inteiro."""
        try:
            int(string)
            return True
        except ValueError:
            return False

    ##### Confiro se o número é válido e já mostro a notícia
    def validate_mostra_manchetes(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Valida opção numérica e mostra notícia."""
        # Olho quantas notícias tem
        tam = len(Fontes.fonte.entries)
        # Limito a 10, se chega a tanto
        if tam > 10: tam = 10
        # Se estiver tudo certinho, mostra a notícia
        if self.is_int(value) and int(value) > 0 and int(value) <= tam:
            num = int(value)
            titulo = Fontes.fonte.entries[num-1].title+' - '+Fontes.fonte.entries[num].published[:16]
            dispatcher.utter_message(text=titulo)
            # Converto para o formato do chat
            texto = markdownify(Fontes.fonte.entries[num-1].summary)
            dispatcher.utter_message(text=texto)
            return {"mostra_manchetes": value}
        else:
            dispatcher.utter_message(template="utter_opcao_estranha_numero")
            return {"mostra_manchetes": None}

    ##### Saída do form
    def submit(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict]:
        """Ao sair pede para escolher próximo, listar ou trocar tipo"""

        dispatcher.utter_message(template="utter_escolha_manchetes_tipo")
        return []



##################### Mostra lista de notícias
class ActionMostraLista(Fontes,Action):
    """Lista as notícias."""

    ##### Inicializa classe e herda parâmetros
    def __init__(self):
        Fontes.__init__(self)

    ##### Nome da action para o Rasa
    def name(self) -> Text:
        return "action_mostra_lista"

    ##### Hora de mostrar a lista de notícias!
    def run(self, dispatcher, tracker, domain) -> Dict[Text, Any]:
        # Aqui são mostrados pelo bot os dados da fonte (título e link)
        dispatcher.utter_message(text="Fonte das notícias:")
        dispatcher.utter_message(text=Fontes.fonte.feed.title+' - '+Fontes.fonte.feed.link)
        dispatcher.utter_message(text="Aí vão as últimas notícias:")
        noticias = ""
        # Quantas notícias são?
        tam = len(Fontes.fonte.entries)
        # Opa, máximo 10
        if tam > 10: tam = 10
        # Coloco toda a lista num string
        for i in range(tam):
            noticias += '{0}) {1} <br /> \n'.format((i+1),Fontes.fonte.entries[i].title)
        # Converto para o formato do chat
        noticias =  markdownify(noticias)
        # Mostro ao usuário
        dispatcher.utter_message(text=noticias)
        # Zero o número da escolha, para entrar legal no form de manchetes
        return [SlotSet("mostra_manchetes",None)]


##################### Segue para a próxima notícia
class ActionProximaOpcao(Action):
    """Passa para próxima notícia."""

    ##### Nome da action para o Rasa
    def name(self) -> Text:
        return "action_proxima_noticia"

    ##### Incremento para a próxima notícia
    def run(self, dispatcher, tracker, domain) -> Dict[Text, Any]:
        # confere se slot de numeração já foi preenchido
        opcao = tracker.get_slot("mostra_manchetes")
        # Se slot não está setado, faço a inicialização
        if opcao == None: opcao = "0"
        # Incremento em 1
        op = int(opcao)+1
        # Se chegou ao final, volta ao primeiro - ficou circular!
        if op > 10: op = 1
        opcao = str(op)
        # Aviso ao usuário
        dispatcher.utter_message(text="Passando para a notícia "+opcao)
        # Zero a escolha, para entrar sereno no form de manchetes
        return [SlotSet("mostra_manchetes",opcao)]


##################### Confere se tipo de notícia está preenchido
class ActionConferePreenchimentoTipo(Fontes,Action):
    """Se não houve preenchimento de tipo, coloca geral."""

    ##### Inicializa classe e herda parâmetros
    def __init__(self):
        Fontes.__init__(self)

    ##### Nome da action para o Rasa
    def name(self) -> Text:
        return "action_confere_preenchimento_tipo"

    ##### Se iniciou sem escolha de tipo, seta o geral como default
    def run(self, dispatcher, tracker, domain) -> Dict[Text, Any]:
        if(tracker.get_slot("escolha_tipo_noticia") == None):
            Fontes.fonte = feedparser.parse(Fontes.geral)
            return [SlotSet("escolha_tipo_noticia", "geral")]
        return []


##################### Limpeza de slot de tipo de notícia
class ActionLimpaSlotTipo(Action):
    """Limpa os slots dependendo da opção escolhida."""

    ##### Nome da action para o Rasa
    def name(self) -> Text:
        return "action_limpa_slot_tipo"

    ##### Zera o slot do tipo
    def run(self, dispatcher, tracker, domain) -> Dict[Text, Any]:
        return [SlotSet("escolha_tipo_noticia", None)]



##################### FIM DE ARQUIVO #####################

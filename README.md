# noticiabot

Projeto de chatbot Rasa para facilitar a leitura de notícias buscadas por RSS.

O desenvolvimento deste projeto pode ser visto em detalhes em https://inteligenciasemantica.wordpress.com/2020/08/25/bot-de-noticias-rss-com-rasa/

![noticiabot](noticiabot_widget.png)

#### Dicas rápidas:

Com o Rasa instalado, para fazer o treinamento:

    rasa train

Para ativar o action server:

    rasa run actions

Em outro terminal, para ativar a API:

    rasa run --enable-api --cors '*'

Ir até a pasta interface e abrir noticiabot.html

---

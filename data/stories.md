<!-------------------------------------------------------------
  Programa: busca de fontes RSS para bot de notícias no Rasa
  Autor: Daniel Nehme Muller
  Criado em: agosto/2020
  Licença: mesma do Rasa
  Arquivo: stories.md - roteiro com a estória dos diálogos,
                        segundo o diagrama de estados pensado
------------------------------------------------------------->

## abertura das notícias
* saudacao
  - action_limpa_slot_tipo
  - tipo_noticia_form
  - form{"name": "tipo_noticia_form"}
  - form{"name": null}
  - action_mostra_lista
  - manchete_form
  - form{"name": "manchete_form"}
  - form{"name": null}

## escolha do tipo de noticia
* tipo_noticia OR noticia_geral OR noticia_economia OR noticia_tecnologia
  - action_limpa_slot_tipo
  - tipo_noticia_form
  - form{"name": "tipo_noticia_form"}
  - form{"name": null}
  - action_mostra_lista
  - manchete_form
  - form{"name": "manchete_form"}
  - form{"name": null}

## lista de notícias
* lista_noticia
  - action_confere_preenchimento_tipo
  - action_mostra_lista
  - manchete_form
  - form{"name": "manchete_form"}
  - form{"name": null}

## próxima noticia
* mais_noticia
  - action_confere_preenchimento_tipo
  - action_proxima_noticia
  - manchete_form
  - form{"name": "manchete_form"}
  - form{"name": null}

<!------------------- FIM DE ARQUIVO ------------------->

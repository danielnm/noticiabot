<!-------------------------------------------------------------
  Programa: busca de fontes RSS para bot de notícias no Rasa
  Autor: Daniel Nehme Muller
  Criado em: agosto/2020
  Licença: mesma do Rasa
  Arquivo: nlu.md - dicionário de intenções e sinônimos
------------------------------------------------------------->

<!---- INTENÇÕES ----->
## intent:saudacao
- oi
- olá
- como vai
- eaí
- salve


## intent:tipo_noticia
- troca
- troco
- troca o tema
- outro tema
- muda
- muda o tema
- tipo
- troca tipo


## intent:noticia_geral
- geral
- notícia
- novidades
- jornal
- últimas


## intent:noticia_economia
- economia
- notícia de economia
- financeiro
- dinheiro
- produção
- econômico
- jornal econômico


## intent:noticia_tecnologia
- tecnologia
- tecno
- informática
- ciência
- digital
- mundo digital
- internet


## intent:numeros
- 1
- 10
- 2
- 3
- 4
- 5
- 6
- 7
- 8
- 9


## intent:mais_noticia
- próxima
- próxima manchete
- próximo
- seguinte
- outra notícia
- manchete
- manchete seguinte
- segue
- vai
- outra
- mostra outra


## intent:lista_noticia
- lista
- lista de notícia
- mostra lista
- vê a lista
- manchetes
- lista as manchetes


<!------------------- FIM DE ARQUIVO ------------------->
